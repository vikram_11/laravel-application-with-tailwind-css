<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\SaveData;

use function GuzzleHttp\Promise\all;

class DataController extends Controller
{
    public function store(Request $request)
    {
        foreach($request->all() as $key => $value)
        {
            $NewData = new SaveData;
            $NewData->data = $value['data'];

        
            $NewData->save();
        }
        

        return response()->json(["message" => "Data saved"],201);
        
    }
    public function index(){

        $books = SaveData::get()->toJson(JSON_PRETTY_PRINT);
        return response($books,200);
    }

}
