<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!v
|
*/

    Route::get('/showdata', [DataController::class,'index'])->name('showdata');
    
    Route::post('/storedata', [DataController::class,'store'])->name('storedata');


    Route::middleware('api')->get('/user', function (Request $request) {
        return $request->user();
    });
